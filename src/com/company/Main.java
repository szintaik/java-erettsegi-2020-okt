package com.company;

import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLOutput;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

    public static List<Sorozat> sorozatok = new ArrayList<>();

    public static void main(String[] args) {
        beolvasas();
        f02();
        f03();
        f04();
        f05();
    }

    private static void f05() {
        System.out.println("\n5. feladat");
        System.out.println("\nAdjon meg egy dátumot! Dátum=");
        Scanner scanner = new Scanner(System.in);
        String beolvasasEredmeny = scanner.next();

        System.out.println(beolvasasEredmeny);
    }

    private static void beolvasas() {
        try{
            // Az összes sort a txt fájlból beolvassuk egy String Listába
            List<String> allLine = Files.readAllLines(Path.of("lista.txt"));

            // Létrehozunk egy tömböt, amiben egy sorozathoz tartozó 5 sort fogjuk tárolni
            // végigmegyünk az előző sorban létrehozott String listán majd és ha az 5 adatot összeszedtük, akkor
            // létrehozunk egy új sorozatot, amit aztán hozzáadunk a globális Sorozatok listához
            String[] sorok = new String[5];

            int i = 0;
            for (String line : allLine) {
                sorok[i] = line;
                i++;

                if(i == 5){
                 i = 0;
                 Sorozat sorozat = new Sorozat(sorok);
                 sorozatok.add(sorozat);
                }
            }
        } catch (Exception e)
        {
            System.err.println(e);
        }

        // Ezzel csak leellenőriztem, hogy mindent sikerült-e betölteni a sorozatok listába
        // ilyenkor, amikor kiíratod, akkor a sorozatok toString metódusa hívódik meg
        // nézd meg a Sorozatok osztályban
        sorozatok.forEach(System.out::println);

        // a fentebb lévővel ekvivalens kiíratások
        // a System.out.println a sorozat .toString metódusát hívja meg alapból, azért nem kell a sorozat.toString()

//        sorozatok.forEach(sorozat -> System.out.println(sorozat));

        for (Sorozat sorozat : sorozatok) {
            System.out.println(sorozat);
        }
    }



    private static void f02() {
        // megoldás stream()-el
        // a stream-et úgy képzeld el, mintha egy for ciklussal végig mennél a listán
        // a listán hívunk egy .stream()-et, ezután lesznek elérhetőek chainelve (hozzáfűzve) a műveletek, amit el szeretnénk végezni a listán
        // elúször hívunk egy filter-t, aminél a sorozat csak egy elnevezés, lehetne kisCica is, aztán -> és egy boolean értéket kell ide összehozni,
        // mintha az if(ide) fejlécébe szertnél írni. lentebb leírom, hogy a Stringeket miért .equals-al kell hasonlítani
        // majd hívunk egy count-ot, hogy a filter után mennyi elem maradt a listában
        // fotnos: az eredeti listát nem szűrte így le!
        // filter művelet: ami igaz az adott elemre, az maradhat, ha nem igaz, akkor kiesel
        long hanyatNemlatott = sorozatok.stream().filter(sorozat -> !sorozat.getAdasbaKerulesDatumString().equals("NI")).count();
        System.out.println("Hányat nem látott: " + hanyatNemlatott);

        // megoldás forEach-el
        int hanyatNemLatott2 = 0;

        // itt a : után a listát vagy tömböt kell tenni, a : elé pedig az első helyre, hogy milyen típusokon megy keresztül, ez lehetne String is, csak
        // nálunk jelen esetben Sorozatok objektumokat tartalmazó listán megyünk végig, utána a sorozat az pedig csak elnevezés, lehetne kisKacsa is, de
        // akkor a forciklus belsejében ezt az elnevezést kellene használnunk
        for (Sorozat sorozat : sorozatok) {
            // így tudjuk ellenőrizni, hogy melyik nem egyenlő az NI-vel, ahogy a Sorozatok construktorában írtam, a Stringeket nem lehet != el összehasonlítani
            // a .equals("NI") visszaad egy boolean értéket és azt kell nekünk letagadni
            if(!sorozat.getAdasbaKerulesDatumString().equals("NI")){
                hanyatNemLatott2++;
            }
        }

        // harmadik megoldás a jó öreg indexes forciklussal, itt csak nehezítés az index, itt a sorozatok.get(i) elemével tudjuk megkapni az egyes elemeket
        // minden iterációban
        int hanyatNemLatott3 = 0;

        for(int i=0; i< sorozatok.size(); i++) {
            if(!sorozatok.get(i).getAdasbaKerulesDatumString().equals("NI")){
                hanyatNemLatott3++;
            }
        }
    }

    private static void f03() {
        //stream megoldás
        float hanyatLatott = sorozatok.stream().filter(Sorozat::getLatta).count();

        System.out.println(sorozatok.size());

        DecimalFormat df = new DecimalFormat("0.00");
        float megoldas = (hanyatLatott / (float)sorozatok.size()) * 100;

        // elso megoldas
        System.out.println("Hany szazalekat latta: " + df.format(megoldas));

        //masodik
        System.out.format("Hany szazalekat latta: %.2f\n", megoldas);

        // alternatív megoldások
        //ugyanaz, mint az első, csak az intellij jelzi, hogy lehet egyszerűsíteni, azért elég a .getLatta(), mert az egy boolean-t ad vissza
        float hanyatLatott2 = sorozatok.stream().filter(kisCica -> kisCica.getLatta()).count();


        // megoldás 3:
        // csak a stream miatt kellett float, amúgy int is jó
        int hanyatLatott3 = 0;

        for (Sorozat sorozat : sorozatok) {
            if(sorozat.getLatta()){
                hanyatLatott3++;
            }
        }
    }


    private static void f04() {
        System.out.println("4. feladat");
        int osszNezettPerc = sorozatok.stream()
                                      .filter(Sorozat::getLatta)
                                      .mapToInt(Sorozat::getHossz)
                                      .sum();

        System.out.println(osszNezettPerc);

        int nezettNapok = osszNezettPerc / 3600;
        int nezettOrak = osszNezettPerc % 3600 / 60;
       int nezettPercek = osszNezettPerc % 60;

        System.out.format("Nap.ora.perc: %d.%d.%d", nezettNapok, nezettOrak, nezettPercek);

    }
}
