package com.company;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Sorozat {

  private String adasbaKerulesDatumString;
  //LocalDate -> év, hónap, napot tárol
  //LocalDateTime -> ugyanezeket + óra, perc, másodperc
  private LocalDate adasbaKerulesDatum;

  // El lehet tárolni őket egyesével is, de úgy bonyolultabb velük dolgozni
  private Integer ev;
  private Integer honap;
  private Integer nap;

  private String cim;
  private Integer evad;
  private Integer resz;
  private Integer hossz;
  private Boolean latta;

  // Amikor létrehozzuk a Sorozatok példányt -> new Sorozat(sorok), akkor ez az úgynevezett constructor
  // fog meghívódni. Ide csak átadjuk a sorok String array-ben összegyűjtött adatokat, majd beletöltjük a változókba
  // itt megcsináljuk a parse-olásokat is
  // egy osztálynak bármennyi construktora lehet, az a lényeg, hogy mások legyenek a bemeneti paraméterei
  // mindegy melyiket használod, ezzel csak azt adod meg, hogy amikor a new Sorozat()-al létrehozod a zárójelben
  // milyen értékeket várjon és hogyan hozza létre az osztályt
  public Sorozat(String[] sorok) {
    if(!sorok[0].equals("NI")) {
      //betöltés LocalDate-be
      this.adasbaKerulesDatum = LocalDate.parse(sorok[0],
          DateTimeFormatter.ofPattern("yyyy.MM.dd"));

      //betöltés egyesével, a . elé a splitben kell a \\, de ezt jelzi is az intellij
      String[] feldaraboltDatum = sorok[0].split("\\.");
      this.ev = Integer.parseInt(feldaraboltDatum[0]);
      this.honap = Integer.parseInt(feldaraboltDatum[1]);
      this.nap = Integer.parseInt(feldaraboltDatum[2]);
    }

    //betoltom azert Stringbe is a példa kedvéért

    this.adasbaKerulesDatumString = sorok[0];


    this.cim = sorok[1];

    // A sorok[2] az egy String és ha .split("x")-et hívunk rajta, akkor az feldarabolja az eredeti Stringet minden x-nél
    // és egy Strig tömböt ad vissza.
    String[] evadEsReszTomb = sorok[2].split("x");
    this.evad = Integer.parseInt(evadEsReszTomb[0]);
    this.resz = Integer.parseInt(evadEsReszTomb[1]);

    this.hossz = Integer.parseInt(sorok[3]);

    // a sorok[4] az egy String és azt vizsgáljuk a .equals("1")-el, hogy 1-e az értéke
    // azért nem == vel vizsgáljuk, mert csak a primitív típusokat tudjuk úgy összehasonlítani pl int, long..., amik kisbetűvel kezdődnek
    // https://www.baeldung.com/java-primitives#:~:text=The%20eight%20primitives%20defined%20in,about%20memory%20management%20in%20Java).
    this.latta = sorok[4].equals("1");
  }

  // pelda az általánosabb construoktorra, ilyenkor már ahol létrehozzuk ott kell előtte parse-olni az értékeket


  public Sorozat(String adasbaKerulesDatumString, LocalDate adasbaKerulesDatum, String cim,
      Integer evad, Integer resz, Integer hossz, Boolean latta) {
    this.adasbaKerulesDatumString = adasbaKerulesDatumString;
    this.adasbaKerulesDatum = adasbaKerulesDatum;
    this.cim = cim;
    this.evad = evad;
    this.resz = resz;
    this.hossz = hossz;
    this.latta = latta;
  }

  // pelda üres contruktorra, ilyenkor minden fieldje (mezője) null-ként jön létre, később tudod be setelni őket, a setter metódusokkal lentebb pl. setAdasbaKerulesDatum(String adasbaKerulesDatum)
  public Sorozat() {
  }

  public String getAdasbaKerulesDatumString() {
    return adasbaKerulesDatumString;
  }

  public void setAdasbaKerulesDatumString(String adasbaKerulesDatumString) {
    this.adasbaKerulesDatumString = adasbaKerulesDatumString;
  }

  public LocalDate getAdasbaKerulesDatum() {
    return adasbaKerulesDatum;
  }

  public void setAdasbaKerulesDatum(LocalDate adasbaKerulesDatum) {
    this.adasbaKerulesDatum = adasbaKerulesDatum;
  }

  public String getCim() {
    return cim;
  }

  public void setCim(String cim) {
    this.cim = cim;
  }

  public Integer getEvad() {
    return evad;
  }

  public void setEvad(Integer evad) {
    this.evad = evad;
  }

  public Integer getResz() {
    return resz;
  }

  public void setResz(Integer resz) {
    this.resz = resz;
  }

  public Integer getHossz() {
    return hossz;
  }

  public void setHossz(Integer hossz) {
    this.hossz = hossz;
  }

  public Boolean getLatta() {
    return latta;
  }

  public void setLatta(Boolean latta) {
    this.latta = latta;
  }

  public Integer getEv() {
    return ev;
  }

  public void setEv(Integer ev) {
    this.ev = ev;
  }

  public Integer getHonap() {
    return honap;
  }

  public void setHonap(Integer honap) {
    this.honap = honap;
  }

  public Integer getNap() {
    return nap;
  }

  public void setNap(Integer nap) {
    this.nap = nap;
  }

  @Override
  public String toString() {
    return "Sorozat{" +
        "adasbaKerulesDatumString='" + adasbaKerulesDatumString + '\'' +
        ", adasbaKerulesDatum=" + adasbaKerulesDatum +
        ", ev=" + ev +
        ", honap=" + honap +
        ", nap=" + nap +
        ", cim='" + cim + '\'' +
        ", evad=" + evad +
        ", resz=" + resz +
        ", hossz=" + hossz +
        ", latta=" + latta +
        '}';
  }
}
